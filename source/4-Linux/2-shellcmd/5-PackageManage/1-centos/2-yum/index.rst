软件包前端管理器yum
======================

使用包管理器安装程序包时有一致命缺陷：无法自动解决程序包之间的依赖关系

为了弥补这一缺陷，包管理器的前端管理器应运而生，它可以自动解决程序包安装过程中的包依赖关系

和包管理器一样，前端管理器也有两种

- \ ``rpm``\ 包管理器对应的前端管理器：\ ``yum``\ 
- \ ``dpkg``\ 包管理器对应的前端管理器：\ ``apt-get``\ 

接下来我们来了解下\ ``rpm``\ 包管理器的前端管理器\ ``yum``\ 

- \ `yum工作机制 <#yuml>`_\ 
- \ `yum配置文件 <#confl>`_\ 
- \ `配置yum仓库源 <#repo>`_\ 

	- \ `配置本地yum源 <#localre>`_\ 
	- \ `配置镜像yum源 <#mirrorre>`_\ 
	- \ `配置第三方yum源 <#thirdre>`_\ 
	- \ `配置专属yum源 <#exclusivel>`_\ 
	- \ `自己创建yum源 <#privatel>`_\ 
- \ `配置yum插件 <#plugin>`_\ 
- \ `yum安装软件包 <#install>`_\ 
- \ `yum卸载软件包 <#remove>`_\ 
- \ `yum升级软件包 <#upgrade>`_\ 
- \ `yum安装包组 <#group>`_\ 
- \ `yum查询信息 <#query>`_\ 



.. _yuml:

0x00 yum工作机制
~~~~~~~~~~~~~~~~~~~~~~~

\ ``yum(Yellowdog Update Modifier)``\ 是基于python开发的程序包管理器\ ``rpm``\ 的前端工具，它会自动解决\ ``rpm命令``\ 安装程序包时无法解决的依赖关系；可以将其理解为\ ``rpm``\ 的一种补充工具，因为\ ``yum``\ 的工作依然要依赖于\ ``rpm``\ 

从实现角度来说：\ ``yum``\ 是一种基于\ ``C/S``\ 架构的软件包管理器的前端工具

- \ ``yum``\ 服务端

	- 实现的功能：本质是一个文件服务器(也可以叫做\ ``yum仓库``\ )，它通过\ ``yum``\ 所支持的文件共享机制将各rpm包通过文件服务共享，即共享rpm包
	- yum服务端(即\ ``yum仓库``\ )存放的内容(以iso镜像为例)有

		- 需要的rpm包，即ISO镜像中\ ``Packages/``\ 目录下所有以\ ``.rpm``\ 结尾的rpm包
		- 需要的元数据文件(各程序包的依赖关系以及各程序包安装后所能够生成的列表等元数据文件)，即ISO镜像中\ ``repodata/``\ 目录下所有内容
	- yum服务端(即\ ``yum仓库``\ )支持的共享协议机制

		- \ ``ftp协议``\ 
		- \ ``http协议``\ 
		- \ ``nfs协议``\ 
		- \ ``file协议``\ 
- \ ``yum``\ 客户端，其工作流程如下

	- 配置相关文件：指定本地客户端可用的yum仓库
	- 下载并缓存元数据：yum从配置文件中指定的yum仓库获取元数据，下载并缓存至本地
	- 分析元数据：根据具体的操作请求完成元数据分析(可能包括依赖关系、文件列表等信息)
	- 执行具体操作(安装、升级卸载等操作)


.. _confl:

0x01 yum配置文件
~~~~~~~~~~~~~~~~~~~~~~~

\ ``yum``\ 的配置文件可分为两类

- 主配置文件：\ ``/etc/yum.conf``\ ，用来配置yum仓库的主配置段，部分定义了全局配置选项
- 仓库配置文件：\ ``/etc/yum.repos.d/*.repo``\ 

	- \ ``/etc/yum.repos.d/CentOS-Base.repo``\ ：用来配置当前系统版本\ ``yum网络仓库``\ (base镜像软件包仓库、updates版本更新仓库、extras其它可用软件包仓库、centosplus插件仓库)
	- \ ``/etc/yum.repos.d/CentOS-Media.repo``\ ：用来配置当前系统版本\ ``yum本地仓库``\ 
	- \ ``/etc/yum.repos.d/CentOS-Vault.repo``\ ：用来配置老系统版本\ ``yum网络仓库``\ 
	- \ ``/etc/yum.repos.d/CentOS-Debuginfo.repo``\ ：用来配置\ ``debug packages仓库``\ 
	- \ ``/etc/yum.repos.d/CentOS-Sources.repo``\ ：用来配置\ ``source packages仓库``\ 
	- \ ``/etc/yum.repos.d/CentOS-fasttrack.repo``\ ：用来配置\ ``fasttrack仓库``\ 
	- \ ``/etc/yum.repos.d/CentOS-CR.repo``\ ：用来配置\ ``CR仓库``\ 


.. _repo:

0x02 配置yum仓库源
~~~~~~~~~~~~~~~~~~~~~~~

\ ``yum仓库源``\ 的配置可分为两段(类似window的ini配置文件)

- \ ``[main]``\ ：主配置段，该段主要是存放在\ ``/etc/yum.conf``\ 主配置文件中，其字段内容如下

	- \ ``cachedir=/var/cache/yum/$basearch/$releasever``\ ：yum缓存下载的rpm包和元数据文件的路径
	- \ ``keepcache=0``\ ：安装完成后是否保留软件包，0为不保留(默认为0)，1为保留
	- \ ``debuglevel=2``\ ：Debug信息输出等级，范围为0-10，默认缺省值为2
	- \ ``logfile=/var/log/yum.log``\ ：yum日志文件位置。用户可以到该路径下去查询过去所做的更新
	- \ ``exactarch=1``\ ：是否只会安装和系统架构匹配的软件包，0为不是，1为是(默认值)
	- \ ``obsoletes=1``\ ：这是一个update的参数，相当于upgrade，允许更新陈旧的RPM包
	- \ ``gpgcheck=1``\ ：是否校验软件包的完整性和来源合法性，1为是，0为不是(默认值)
	- \ ``plugins=1``\ ：是否启用插件，1为允许(默认值)，0为不允许
	- \ ``installonly_limit=5``\ 
	- \ ``bugtracker_url=http://bugs.centos.org/set_project.php?project_id=23&ref=http://bugs.centos.org/bug_report_page.php?category=yum``\ 
	- \	``distroverpkg=centos-release``\ 
- \ ``[repo_ID]``\ ：仓库配置段(每个仓库的\ ``repo_ID``\ 是唯一的，不能重复使用)，该段是存放在\ ``/etc/yum.repos.d/*.repo``\ 仓库配置文件中，其字段内容如下

	- \ ``name=对repository的描述``\ ：支持像\ ``$releasever``\ 、\ ``$basearch``\ 这样的变量
	- \ ``baseurl=仓库的访问路径``\ ：即\ ``repodata``\ 目录所在目录为仓库路径，可支持的路径格式如下

		- \ ``ftp://server/path/to/repo``\ 
		- \ ``http://server/path/to/repo``\ 
		- \ ``nfs://servr/nfs_path``\ 
		- \ ``file:///path/to/repo``\ 
	- \ ``enabled={1|0}``\ ：\ ``1``\ 表示启用，\ ``0``\ 表示不启用
	- \ ``gpgcheck={1|0}``\ ：\ ``1``\ 表示验证\ ``gpg``\ ，\ ``0``\ 表示不验证
	- \ ``gpgkey=公钥地址``\ ：该公钥地址可以是本地，也可以是服务器端路径，可支持的路径格式如下

		- \ ``ftp://server/path/to/gpgkey``\ 
		- \ ``http://server/path/to/gpgkey``\ 
		- \ ``nfs://servr/nfs_path``\ 
		- \ ``file:///path/to/gpgkey``\ 
	- \ ``failovermethod={priority|roundrobin(默认值)}``\ 

		- \ ``roundrobin``\ ：表示有多个url可供选择时，yum会随机选择，如果连接失败则使用下一个，依次循环
		- \ ``priority``\ ：表示有多个url可供选择时，yum会根据url的次序从第一个开始
	- \ ``cost=此仓库开销``\ ：默认为\ ``1000``\ ，开销越小的仓库将被优先使用

上述配置字段中可用的宏有

- \ ``$releasever``\ ：代表发行版的版本，从\ ``[main]``\ 部分的\ ``distroverpkg``\ 获取；如果没有，则根据\ ``redhat-release包``\ 进行判断，且只替换为主版本号，如\ ``Redhat6.5``\ 则替换为\ ``6``\ \
- \ ``$arch``\ ：代表系统架构，如\ ``i686/i586/x86_64``\ 
- \ ``$basharch``\ ：代表系统基本架构，如\ ``i686/i586``\ 等的基本架构为\ ``i386``\ 
- \ ``$YUM0-9``\ ：在系统中定义的环境变量，可以在yum中使用

由于yum是基于python开发的，可以通过以下方式来获取当前系统yum仓库配置时可使用的宏替换

.. figure:: ../images/19.png

.. _localre:

0x0200 配置本地yum源
~~~~~~~~~~~~~~~~~~~~~~~

配置本地yum源的流程是

- 挂载系统安装镜像iso

.. figure:: ../images/20.png

.. figure:: ../images/21.png

- 编辑\ ``/etc/yum.repos.d/CentOS-Media.repo``\ 文件配置本地yum源

.. figure:: ../images/22.png

- 禁用默认的yum网络源：将yum网络源配置文件改名为\ ``CentOS-Base.repo.bak``\ 或者将该配置文件中的所有网络仓库的enable的值都改为0，否则会先在网络源中寻找适合的包，改名之后直接从本地源读取

.. figure:: ../images/23.png

\ **注意**\ ：用镜像创建的本地仓库中的rpm包来自于镜像iso文件中，不能进行更新升级，所以不建议使用本地yum源


.. _mirrorre:

0x0201 配置镜像yum源
~~~~~~~~~~~~~~~~~~~~~~~

配置镜像yum源的流程是

- 选择适合自己的镜像yum源

	- \ `阿里aliyun <https://mirrors.aliyun.com/centos/>`_\ 
	- \ `网易163 <http://mirrors.163.com/centos/>`_\ 
	- \ `搜狐sohu <http://mirrors.sohu.com/centos/>`_\ 
	- \ `清华大学 <https://mirrors.tuna.tsinghua.edu.cn/centos/>`_\ 
	- \ `中国科技大学 <http://mirrors.ustc.edu.cn/centos/>`_\ 
	- \ `东软信息学院 <http://mirrors.neusoft.edu.cn/centos/>`_\ 
- 编辑配置文件\ ``/etc/yum.repos.d/CentOS-Base.repo``\ 中的各仓库,将官方提供的\ ``mirrorlist``\ 项注释掉
- 修改\ ``baseurl``\ 项的值(该值是\ ``reposdata``\ 目录所在的目录路径；由于在配置文件中使用了宏，所有可以直接将\ ``http://mirror.centos.org/``\ 改为自己选择的网络源的根目录，即centos的上一级目录)
- 将\ ``enable``\ 项的值都设为\ ``1``\ 
- 启用\ ``gpgcheck``\ 校验
- 指定\ ``gpgkey``\ 的路径

.. figure:: ../images/24.png

- 也可以通过编辑以下仓库来实现镜像yum源的配置，配置方法如下

	- \ ``/etc/yum.repos.d/CentOS-CR.repo``\ 文件中的\ ``CR仓库``\ 
	- \ ``/etc/yum.repos.d/CentOS-fasttrack.repo``\ 文件中的\ ``fasttrack仓库``\ 


.. _thirdre:

0x0202 配置第三方yum源
~~~~~~~~~~~~~~~~~~~~~~~

发行版光盘iso以及镜像站点提供的rpm软件包都是常规包，非常有限，有时无法满足我们的需要；此时可以将第三方rpm软件包库配置成yum源

配置第三方yum源的流程是

- 选择适合自己的第三方rpm软件包库

	- \ `Fedora epel <https://dl.fedoraproject.org/pub/epel/>`_\ 
	- \ `阿里epel <https://mirrors.aliyun.com/epel/>`_\ 
	- \ `rpmfusion <http://download1.rpmfusion.org/free/el/>`_\ 
	- \ `阿里rpmfusion <https://mirrors.aliyun.com/rpmfusion/free/el/>`_\ 
- 使用\ ``wget``\ 命令下载对应版本的\ ``-release.rpm包``\ 以及\ ``gpgkey文件``\ 
	
	- \ ``epel``\ 源对应的\ ``-release.rpm包``\ 和\ ``gpgkey文件``\ 分别是：\ ``epel-release-latest-7.noarch.rpm``\ 和\ ``RPM-GPG-KEY-EPEL-7``\  
	- \ ``rpmfusion``\ 源对应的\ ``-release.rpm包``\ 和\ ``gpgkey文件``\ 分别是：\ ``rpmfusion-free-release-7.noarch.rpm``\ 和\ ``RPM-GPG-KEY-EPEL-7``\ 

.. figure:: ../images/25.png

.. figure:: ../images/26.png

- 使用\ ``rpm --import``\ 命令导入下载好的\ ``gpgkey文件``\ 

.. figure:: ../images/27.png

- 使用\ ``rpm -K /path/to/rpmpackage``\ 命令校验其完整性和来源合法性

.. figure:: ../images/28.png

- 使用\ ``rpm -ivh /path/to/rpmpackage``\ 安装对应版本的\ ``-release.rpm包``\ 

.. figure:: ../images/29.png

- 安装完后会在\ ``/etc/yum.repo.d/``\ 目录下生成对应的\ ``.repo``\ 仓库配置文件；编辑对应的仓库配置文件就可以配置对应的第三方yum源

.. figure:: ../images/30.png

.. figure:: ../images/31.png


.. _exclusivel:

0x0203 配置专属yum源
~~~~~~~~~~~~~~~~~~~~~~~

配置专属yum源可参考各自软件包提供商官网

配置\ ``docker源``\ 的流程如下

- 使用\ ``cd /etc/yum.repos.d/``\ 命令进入仓库配置文件目录下
- 使用\ ``vim doker-engine.repo``\ 命令创建并编辑\ ``docker``\ 对应的yum源仓库配置文件

.. figure:: ../images/32.png

- 使用\ ``yum install docker``\ 命令安装\ ``docker``\ 

.. figure:: ../images/33.png


.. _privatel:

0x0204 自己创建yum源
~~~~~~~~~~~~~~~~~~~~~~~

自己创建yum源的流程如下

- 熟悉\ ``yum源仓库``\ 的组成部分

	- \ ``packages``\ ：各rpm包
	- \ ``repodata``\ ：各元数据信息

.. figure:: ../images/34.png

- 配置yum服务端

	- 使用\ ``yum install createrepo``\ 命令安装\ ``createrepo``\ 工具，该工具是用来生成\ ``各rpm包的元数据信息repodata``\ 
	- 使用\ ``yum install httpd``\ 命令安装\ ``httpd``\ 服务，该服务是用来将\ ``rpm包和生成的repodata配置成文件服务器``\ 
	- 在\ ``/var/www/html/``\ 目录下，创建一个目录\ ``test``\ 来保存\ ``准备制作成为yum仓库的所有rpm包``\ 
	- 使用\ ``createrepo /var/www/html/test/``\ 命令创建yum仓库
	- 使用\ ``service httpd start``\ 命令启动http服务
	- 使用\ ``netstat -pantu``\ 命令查看是否侦听80端口，如果是则启动http服务成功
- 配置yum客户端

	- 在\ ``/etc/yum.repo.d/``\ 目录下新建一个\ ``test.repo``\ 仓库配置文件，编辑此配置文件并配置
	- 使用\ ``yum repolist``\ 命令查看\ ``test仓库``\ 是否可用
	- 有时需要启用网络yum源和epel第三方yum源来补充自建yum源缺省的依赖软件包
	- 安装软件包测试

.. _plugin:

0x03 配置yum插件
~~~~~~~~~~~~~~~~~~~~~~~

- 配置\ ``yum-priorities``\ 插件：该插件是用来设置yum在调用软件源时的顺序

	- 使用\ ``yum install yum-priorities``\ 命令安装插件
	- 编辑\ ``/etc/yum/pluginconf.d/priorities.conf``\ 配置文件，修改\ ``enable``\ 项的值为\ ``1``\ ，启用该插件
	- 编辑\ ``/etc/yum.repos.d/*.repo``\ 仓库配置文件，在需要指定优先级的仓库中添加\ ``priority=N``\ 项

		- \ ``N``\ 为\ ``1~99``\ 的正整数，数值越小越优先
		- 一般配置\ ``[base]/[addons]/[updates]/[extras]``\ 的\ ``priority=1``\ 
		- 一般配置\ ``[CentOSplus]/[contrib]``\ 的\ ``priority=2``\ 
		- 一般配置其他第三方的软件源为\ ``priority=N``\ (推荐\ ``N>10``\ )

.. figure:: ../images/35.png

.. figure:: ../images/36.png

.. figure:: ../images/37.png

- 配置\ ``yum-fastestmirror``\ 插件：该插件可以自动选择最快的yum源

	- 使用\ ``yum install yum-fastestmirror``\ 命令安装插件
	- 编辑\ ``/etc/yum/pluginconf.d/fastestmirror.conf``\ 配置文件，修改\ ``hostfilepath``\ 项的值为\ ``timedhosts.txt``\ 
	- 编辑\ ``/var/cache/yum/timedhosts.txt``\ 文件，此时可以将所知道的yum源统统写入这个txt文件；这样，以后我们在yum安装rpm包的时候，就可以让yum自己去选择最快的yum源了

- 配置\ ``yum-axelget``\ 插件：该插件是基于yum下的一个多线程下载插件，通过打开多个\ ``HTTP/FTP``\ 连接来将一个文件进行分段下载，从而达到加速下载的目的；对于下载大文件，该工具特别有用

	- 使用\ ``yum install yum-axelget``\ 命令安装插件
	- 插件安装后可以编辑生成的\ ``/etc/yum/pluginconf.d/axelget.conf``\ 配置文件来设定下载属性

.. figure:: ../images/38.png

.. figure:: ../images/39.png

- \ **注意**\ ：\ ``axel``\ 还可以当做独立的多线程下载工具来使用；速度比\ ``wget``\ 要快

.. figure:: ../images/40.png

.. _install:

0x04 yum安装软件包
~~~~~~~~~~~~~~~~~~~~~~~

yum安装软件包时只需要指定包名即可，它只是解决了rpm包安装时的依赖关系，所以安装的依然是rpm格式的软件包；yum安装软件包相关命令有

- \ ``yum install package_name``\ ：安装指定的程序包
- \ ``yum reinstall package_name``\ ：重新安装指定的包


.. _remove:

0x05 yum卸载软件包
~~~~~~~~~~~~~~~~~~~~~~~

yum卸载软件包时依赖于此软件包的包也会被卸载，yum卸载软件包相关命令有

- \ ``yum erase package_name``\ 
- \ ``yum remove package_name``\ 

.. _upgrade:

0x06 yum升级软件包
~~~~~~~~~~~~~~~~~~~~~~~

yum升级软件包相关命令有

- 使用\ ``yum check-update``\ 命令检查可升级的包
- 使用\ ``yum update [package_name.....]``\ 命令升级指定的软件包；如果不指定任何软件包，则会升级所有可升级的软件包；如果需要升级软件包到指定版本要带版本号
- 使用\ ``yum downgrade packages_name``\ 命令可降级指定的软件包


.. _group:

0x07 yum安装包组
~~~~~~~~~~~~~~~~~~~~~~~

yum安装包组相关命令有

- 使用\ ``yum grouplist``\ 命令列出所有包组信息
- 使用\ ``yum groupinfo "package_group_name"``\ 命令查看指定包组的详细信息(注意包组名中间会有空格，需要用引号将其引起来)；下面列出了三个跟开发相关的包组

	- \ ``Desktop Platform Development``\ 
	- \ ``Server Platform Development``\ 
	- \ ``Development Tools``\ 
- 使用\ ``yum groupinstall "package_group_name"``\ 命令可安装包组
- 使用\ ``yum groupremove "package_group_name"``\ 命令可卸载包组


.. _query:

0x08 yum查询信息
~~~~~~~~~~~~~~~~~~~~~~~

yum查询信息相关命令有

- \ ``yum repolist [all|enabled(已启用-默认值)|disabled(已禁用)]``\ ：列出所有可用的yum源
- \ ``yum clean [all|packeages(包)|metadata(元数据)|expire-cache(过期数据)|rpmdb(rpm数据)|plugins]``\ ：清除缓存
- \ ``yum list [all|installed(已安装过的)|available(可用)]``\ ：列出yum包信息
- \ ``yum info {package_name|package_group_name}``\  ：显示关于软件包或组的详细信息
- \ ``yum {whatprovides|provides} /path/to/somefile``\ ：查询某文件是由谁提供的
- \ ``yum history``\ ：查看yum的命令历史
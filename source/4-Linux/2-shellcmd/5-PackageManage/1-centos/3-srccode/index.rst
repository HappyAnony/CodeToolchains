源码编译
=================

虽然yum可以自动解决程序包安装时之间的依赖关系，但是其本质依然是安装rpm包，因此安装的软件包依然不是最新的，如果想要安装最新版本的软件包，可以通过源码编译进行安装

- \ `相关概念 <#conceptl>`_\ 
- \ `编译环境准备 <#develenv>`_\ 
- \ `下载源代码 <#downloadll>`_\ 
- \ `解压源代码 <#unpackl>`_\ 
- \ `执行configure <#configurell>`_\ 
- \ `编译源代码 <#makel>`_\ 
- \ `安装编译结果 <#makeinstall>`_\ 
- \ `安装后配置 <#makeconfl>`_\ 



.. _conceptl:

0x00 相关概念
~~~~~~~~~~~~~~~~~~~~~~~

常见的源码类型有

- \ ``c源码``\ 
- \ ``c++源码``\ 
- \ ``perl源码``\ 
- \ ``python源码``\

linux应用程序一般来自以下项目组

- 软件基金会：\ ``FSF``\ 、\ ``ASF``\ 
- 项目组：发起者+contributor(贡献者)
- 小项目：一个人开发维护(\ `github <https://github.com/>`_\ 、\ `gitlab <https://about.gitlab.com/>`_\ 、\ `coding <https://coding.net/>`_\ )
- 商业公司支持：开源版、企业版

linux应用程序的源码包通常包括以下组件

- \ ``打包压缩好的源码包``\ ：通常会把代码分散于多个源代码文件，此时需要将其打包压缩
- \ ``configure脚本``\ ：该脚本是用来检查编译环境是否满足编译需求，并定义当前程序编译时启用那个特性或功能，以及安装路径的定义等等；该脚本是使用\ ``autoconf``\ 工具在源代码中生成的；它会根据\ ``makefile.in``\ 文件生成\ ``makefile``\ 文件(\ ``makefile``\ 文件是\ ``make``\ 自动化编译源码的配置文件)
- \ ``Makefile.in文件``\ ：该文件是用于结合\ ``configure脚本``\ 生成\ ``makefile文件``\ ,该文件是由\ ``automake工具``\ 生成的
- \ ``INSTALL``\ 或\ ``README``\ 安装说明文件：基本上所有源码包都会提供这些安装说明文档

各个应用程序源码编译安装的方法步骤可能会存在差异，在编译安装之前最好使用\ ``less INSTALL``\ 或者\ ``less README``\ 命令查看源码包提供的说明文档，根据说明文档进行安装

以下是常用的源码编译安装步骤流程，只供参考

.. _develenv:

0x01 编译环境准备
~~~~~~~~~~~~~~~~~~~~~~~

进行源码编译之前，需要配置一些编译环境，CentOS系统下可以通过以下命令安装编译开发环境

- 使用\ ``yum groupinstall "Development Tools"``\ 命令安装开发工具包组准备相关的开发编译环境
- 使用\ ``yum groupinstall "Desktop Platform Development"``\ 命令安装桌面平台开发工具包组
- 使用\ ``yum groupinstall "Server Platform Development"``\ 命令安装服务器平台开发工具包组

.. _downloadll:

0x02 下载源代码
~~~~~~~~~~~~~~~~~~~~~~~

我们可以通过\ ``wget``\ 和\ ``axel``\ 命令下载源代码

.. code-block:: sh

	# 使用wget命令下载
	$ wget http://nginx.org/download/nginx-1.10.3.tar.gz

	# 使用axel多线程下载工具下载，速度比wget要快
	$ axel -o /path/to/out -n Number(线程数) http://nginx.org/download/nginx-1.10.3.tar.gz

.. figure:: ../images/42.png

.. _unpackl:

0x03 解压源代码
~~~~~~~~~~~~~~~~~~~~~~~

我们一般通过\ `tar <../../../2-FileManage/3-FileOperate/2-Dir/index.html#tarl>`_\ 命令来解压源代码

.. code-block:: sh

	$ tar -zvxf package-version.tar.gz
	$ tar -jvxf package-version.tar.bz2
	$ tar -Jvxf package-version.tar.xz

	# 展开后的目录通常为package-version

.. figure:: ../images/43.png

.. figure:: ../images/44.png

.. _configurell:

0x04 执行configure脚本
~~~~~~~~~~~~~~~~~~~~~~~

\ ``configure脚本``\ 是为编译安装做准备的，不同的程序，其\ ``configure脚本``\ 不尽相同，可以通过\ ``./configure --help``\ 命令获取帮助信息，其功能大致有

- 编译前检测：检查编译环境是否满足编译需求

	- make编译环境是否已安装就绪
	- make编译所依赖的软件包(\ ``PCRE library``\ )是否已就绪；该软件包一般是指依赖此软件包中的开发环境，而软件包为了管理有主包和支包之别，一般开发环境都在\ ``package_name-devel支包``\ 中
- 编译时配置：配置相关特性，保存到\ ``./configure``\ 命令结合\ ``makefile.in``\ 文件生成的\ ``makefile``\ 文件中

	- 指定启动/禁用的特性，保存到\ ``./configure``\ 命令结合\ ``makefile.in``\ 文件生成的\ ``makefile``\ 文件中

		- \ ``--enable-feature``\ ：例如\ ``--enable-fpm``\ 表示启用fpm特性
		- \ ``--disable-fecture``\ ：例如\ ``--disable-socket``\ 表示启用socket特性
	- 指定所依赖功能、程序或文件

		- \ ``--with-function``\ 表示启用某功能
		- \ ``--without-function``\ 表示禁用某功能
- 编译后安装：指定源码安装路径，保存到\ ``./configure``\ 命令结合\ ``makefile.in``\ 文件生成的\ ``makefile``\ 文件中

	- \ ``--prefix=/usr/local/package_name``\ ：用于指定安装路径
	- \ ``--sysconfdir=/etc/packagename/packagename.conf``\ ：用于指定配置文件的存放路径


执行\ ``configure脚本``\ 的步骤如下

.. code-block:: sh


	# 切换到源码目录
	$ cd /path/to/package-version

	# 执行configure脚本
	# 安装路径以及启用功能根据自己需求选择后填写上去；具体实现选项参数可参考./configure --help命令获取的帮助信息
	$ ./configure --prefix=PATH --conf-path=PATH [....]

.. figure:: ../images/45.png

.. figure:: ../images/46.png

.. figure:: ../images/47.png

在执行过程中可能会报错，只要按照提示安装指定的依赖组件，然后重启执行configure脚本即可；例如下面就有一个报错信息，缺少\ ``PCRE_library``\ 库，于是就找到\ ``pcre-devel``\ 支包安装，然后重新执行configure脚本即可(编译所需的依赖环境一般是指依赖某软件包中的开发环境，而软件包为了管理有主包和支包之别，一般开发环境都在\ ``package_name-devel``\ 支包中)

.. figure:: ../images/48.png

.. figure:: ../images/49.png

解决依赖环境后，重新执行configure脚本，出现以下提示信息表示脚本执行成功

.. figure:: ../images/50.png

configure脚本执行成功后会在当前目录下生成\ ``makefile``\ 文件，供make工具编译安装使用

.. figure:: ../images/51.png

.. _makel:

0x05 编译源代码
~~~~~~~~~~~~~~~~~~~~~~~

直接执行\ ``make``\ 命令就会根据执行configure脚本生成的makefile配置文件进行编译

.. figure:: ../images/52.png

.. _makeinstall:

0x06 安装编译结果
~~~~~~~~~~~~~~~~~~~~~~~

直接执行\ ``make install``\ 命令即可完成编译结果的安装

.. figure:: ../images/53.png

.. _makeconfl:

0x07 安装后配置
~~~~~~~~~~~~~~~~~~~~~~~

源码编译安装后一般需要做以下配置

- 输出二进制程序：将二进制程序安装路径添加到\ ``PATH``\ 环境变量中，这样不用指定绝对路径，直接输入软件名就可以运行该软件

	- 使用\ ``vim /etc/profile.d/packages_name.sh``\ 命令在\ ``/etc/profile.d``\ 目录下创建以\ ``.sh``\ 结尾、\ ``package_name``\ 为文件名的脚本文件
	- 向该脚本文件添加\ ``export PATH=/usr/local/packages_name/bin(软件的二进制程序所在目录;下同):/usr/localpackages_name/sbin:$PATH``\ 
	- 使用\ ``source /etc/profile.d/packages_name.sh``\ 命令重读\ ``PATH``\ 环境变量
	- 使用\ ``echo $PATH``\ 命令查看配置是否生效
- 输出库文件：不用指定绝对路径,系统可直接调用该软件的库文件

	- 使用\ ``vim /etc/ld.so.conf.d/packages_name.conf``\ 命令在\ ``/etc/ld.so.conf.d/``\ 目录下创建以\ ``.conf``\ 结尾、\ ``package_name``\ 为文件名的配置文件
	- 向该脚本文件添加\ ``/usr/local/packages_name/lib``\ ，用来直接指定软件库文件的所在路径
	- 使用\ ``ldconfig -v | grep "[^[:space:]]"``\ 命令让系统重新生成库文件路径缓存\ ``/etc/ld.so.cache``\ 
- 输出头文件：不用指定绝对路径,系统可直接调用该软件的头文件

	- 直接通过创建符号链接来实现输出头文件：\ ``ln -sv /usr/local/package_name/include(软件头文件存放路径) /usr/include/package_name``\ 
- 输出配置文件：便于管理配置文件，遵循FHS标准

	- 使用\ ``./configure``\ 命令执行脚本时，在输入参数中直接指定配置文件的存放路径即可
- 输出帮助文档：不用指定绝对路径，直接输入man COMMAND命令就可以得到帮助文档

	- 使用\ ``vim /etc/man_db.conf``\ 命令编辑该配置文件，在\ ``MANPATH``\ 字段添加如下内容：\ ``MANPATH /usr/local/package_name/man``\ (软件man文档的存放路径)
#-*- coding:utf-8 -*-
#author:anony

import os
import sys

# 当前python文件所在的目录
py_dir = os.path.dirname(os.path.abspath(__file__))

# 平台标志位：0代表Window平台，1代表类Unix平台
arch_flag = 0

# 写rst文件目录树标志位：0代表不写，用于之后添加章节；1代表写，用于第一次初始化章节
info_flag=1

# index文件名
index = "index.rst"

# 回调函数目录
func = {
	"0": "dir_init()",
	"1": "text_init()"
}


# 目录结构
dir_tree = {
	"1-basicopl": "磁盘基本操作",
	"2-lvm": "LVM逻辑卷",
	"3-raid": "RAID磁盘阵列"
}


# 内容结构
text_tree = {
	"yuml": "yum工作机制",
	"confl": "yum配置文件",
	"repo": "配置yum仓库源",
	"localre": "配置本地yum源",
	"mirrorre": "配置镜像yum源",
	"thirdre": "配置第三方yum源",
	"exclusivel": "配置专属yum源",
	"privatel": "自己创建yum源",
	"plugin": "配置yum插件",
	"install": "yum安装软件包",
	"remove": "yum卸载软件包",
	"upgrade": "yum升级软件包",
	"group": "yum安装包组",
	"query": "yum查询信息",
}

text_pre_tree = {}
text_post_tree = {}
text_file = "index.rst"


# rst文件的目录树
info = '''



.. toctree::
   :titlesonly:
   :glob:


'''

# rst文件中一级标题
title_sign = "=================\n"


def write_data(file_name, data):
	"""
	向指定文件写入数据
	:param file_name:
	:param data:
	:return:
	"""
	ret = True
	try:
		with open(file_name, "a", encoding="utf-8") as f:
			f.write(data)
	except Exception as e:
		print(e)
		ret = False
	finally:
		return ret


def dir_create(dir_name):
	"""
	创建目录
	:param dir_name:
	:return:
	"""
	ret = True
	try:
		os.mkdir(dir_name)
	except FileExistsError as e:
		print(e)
		ret = False
	except Exception as e:
		print(e)
		ret = False
	finally:
		return ret


def dir_change(dir_name):
	"""
	切换工作目录
	:param dir_name:
	:return:
	"""
	ret = True
	try:
		os.chdir(dir_name)
	except Exception as e:
		print(e)
		ret = False
	finally:
		return ret


def dir_init():
	if info_flag:
		write_data(index, info)
	else:
		write_data(index, '\n')
	for key in dir_tree:
		tmp_dir = os.path.abspath(__file__)
		if (tmp_dir != py_dir) and dir_change(py_dir):
			data = "   " + key + "/index" + '\n'
			write_data(index, data)
			if not dir_create(key):
				print(key, "创建失败")
				return False
			if dir_change(key):
				data = dir_tree[key] + '\n'
				if not (write_data(index, data) and write_data(index, title_sign)):
					print(key, "/index 写入失败")
					return False
			else:
				print(key, "切换失败")
				return False
		else:
			print(py_dir, "切换失败")
			return False
	return True

# 	"rpml": "rpm包管理器",
# - \ `rpm包管理器 <#rpml>`_\ 
# .. _rpml:

#0x00 rpm包管理器
#~~~~~~~~~~~~~~~~~~

def text_tree_init():
	ten = 0
	bit = 0
	for tag in text_tree:
		if bit >= 9:
			bit = 0
			ten += 1
		text_pre_tree["- \ `" + text_tree[tag]] = " <#" + tag + ">`_\ \n"
		text_post_tree[".. _" + tag + ":\n\n"] = "0x" + str(ten) + str(bit) + " " + text_tree[tag] + "\n"
		bit += 1


def pre_tree_write(file):
	for key in text_pre_tree:
		data = key + text_pre_tree[key]
		if not write_data(file, data):
			return False
	return True


def post_tree_write(file):
	for key in text_post_tree:
		data = key + text_post_tree[key] + "~~~~~~~~~~~~~~~~~~~~~~~\n\n\n\n"
		if not write_data(file,data):
			return False
	return True


def text_init():
	text_tree_init()
	if not pre_tree_write(text_file):
		print("text_init fail")
		return False
	write_data(text_file, "\n\n\n")
	if not post_tree_write(text_file):
		print("text_init fail")
		return False
	return True


def main():
	func_index = str(sys.argv[1])
	if not eval(func[func_index]):
		return False
	return True


if __name__ == '__main__':
	if not main():
		print("\033[31;1mbulid fail\033[0m")


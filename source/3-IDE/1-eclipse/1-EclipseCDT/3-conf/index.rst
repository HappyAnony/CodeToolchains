基本配置
=================

- \ `映射工作目录 <#workl>`_\ 
- \ `配置常用选项 <#optionsll>`_\ 
- \ `安装svn插件 <#svnl>`_ \
- \ `添加svn资源库 <#svncl>`_\ 


.. _workl:

0x00 映射工作目录
~~~~~~~~~~~~~~~~~~

为了方便编译，我们一般采用本地window使用\ ``Eclipse CDT``\ 修改代码，远程linux服务器\ ``gcc``\ 编译的方式

此时就需要将安装\ ``samba``\ 服务的linux服务器上的目录映射到本地进行编辑

.. figure:: ../images/19.png

.. _optionsll:

0x01 配置常用选项
~~~~~~~~~~~~~~~~~~

打开\ ``eclipse``\ ，选择自己默认的工作区间

- \ **此时将默认工作区间改为刚才映射的工作目录**\ ，因为之后我们检出项目时可以使用默认的工作区间，实现本地和服务器的代码同步
- 可以重新配置默认工作区间，参考\ `Eclipse如何修改eclipse默认的工作空间路径 <https://blog.csdn.net/songyi160/article/details/53844311>`_\ 

	- 选择\ ``Window->Preferences->General->Startup and Shutdow->Workspace``\ 
	- 勾选\ ``Prompt for workspaces on startup``\ 
	- 重启\ ``Eclipse``\ ，会和首次启动\ ``Eclipse``\ 一样，弹窗提示设置默认工作区间
- 可以随便切换工作空间：选择\ ``File->Switch Workspace->Other``\ ，可以随时切换到其他工作空间或创建新的工作空间

.. figure:: ../images/41.png

第一次启动后界面如下，点击右上角的\ ``Workbench``\ 按钮进入工作空间

.. figure:: ../images/21.png

点击菜单栏\ ``Windows-->Preferences``\ ，可以配置相关属性

.. figure:: ../images/22.png

点击\ ``General->Appearance``\ 选择喜欢的界面主题

- 个人感觉\ ``eclipse``\ 的默认主题太丑，安装了个主题插件，可参考：\ `设置漂亮的eclipse主题风格 <https://www.cnblogs.com/java1710-tare-1341550692-html/p/8064462.html>`_\ 

.. figure:: ../images/23.png

点击菜单栏\ ``Help->Eclipse Marketplace``\ ，搜索\ ``color eclipse themes``\ ，安装\ ``eclipse color theme``\ 插件就行

.. figure:: ../images/31.png

.. figure:: ../images/46.png

.. figure:: ../images/47.png

安装好之后重启\ ``eclipse``\ 打开偏好设置\ ``General->Preferences``\ , 选择喜欢的theme主题即可

.. figure:: ../images/48.png


设置字体

.. figure:: ../images/24.png

设置默认文件编码格式及换行符

.. figure:: ../images/25.png

设置用空格替换制表符

.. figure:: ../images/26.png

.. figure:: ../images/27.png

.. figure:: ../images/28.png

设置最大行数限制(查看超过该限制的文件时将会关闭部分语法着色功能)

.. figure:: ../images/29.png

设置常用功能快捷键

.. figure:: ../images/30.png

.. _svnl:

0x02 安装svn插件
~~~~~~~~~~~~~~~~~~

点击菜单栏\ ``Help->Eclipse Marketplace``\ ，搜索\ ``SVN``\ ，安装\ ``Subclipse``\ 插件

.. figure:: ../images/31.png

.. figure:: ../images/32.png

.. figure:: ../images/33.png

安装完成后重启\ ``eclipse``\ 

.. figure:: ../images/34.png

设置\ ``SVN``\ 接口为\ ``SVNKit``\ 

.. figure:: ../images/35.png

.. _svncl:

0x03 添加svn资源库
~~~~~~~~~~~~~~~~~~~~

点击右上角\ ``Open Perspective``\ 按钮，打开\ ``SVN资源库视图``\ 

.. figure:: ../images/36.png

点击\ ``svn资源库研究``\ ，右键\ ``reset``\  可重置svn资源库，获取相关操作界面

.. figure:: ../images/37.png

.. figure:: ../images/38.png

点击\ ``添加SVN资源库``\ 按钮，输入要添加的\ ``代码svn路径``\ 

.. figure:: ../images/39.png

输入SVN账号和密码之后就可以查看已添加的svn资源库的内容

.. figure:: ../images/40.png


